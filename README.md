# assignment-5

## Getting started
You Need these to make the project work: 
- SQL SERVER MANAGEMENT STUDIO
- VISUAL STUDIO 2022

## Name
Data Access With SQL Client

## Description
This assignment is in two parts. Appendix A files will be in the the "AppendixA" folder. Appendix B project will be in the "AppendixB" Folder. With the files from Appendix A you'll create a database with 4 tables (3 created from start and 1 linking table). With the project in Appendix B you'll see multiple models and a repository to access and modify a database with SQL Server. To make the project in Appendix B work, you'll need to have the Chinook database created in SSMS

## Choices made for appendix b methods 

### Read a specific customer by Name
For this part I decided to pass a string with the full name. For this I just assumed that no one has a middle name or a double last name. The string with the name will be split into an array, and the first to elements in the array will be passed to the query as firstname and lastname 

### Return a page of customers from the database 
The limit and offset statement did not work for me in this part. Instead I used the FETCH and First Rows only so we could use limit and offset in this query somehow. 

### Add a new Customer to the database
For this method I decided to send all values that are in the model so that the customer added had all the values. I could add some functions so that it checks if I send a null value in. 

### Update an existing customer 
To get the existing customer I used the id that I made in the customer object from program.cs.

### Return the number of customers in each country 
Made a model with CustomerCountry that holds the country and amount of customers from the country. Made a query that returns the countries with the amount of customers on each row. The method then takes the result and goes through every line and adds the parameters to a customerCountry object. The list than gets returned with every object that was added 

### Customers who are the highest spenders 
Very similar to the last method. Made a model called CustomerSpenders and the query returned a list of the spenders. A functionality I could've added would've been to get the top 10 instead of every single person and their spending. 

### For a given customer, their most popular genre. 
This returns a CustomerGenre object with a customer name and their most popular genre. For simplicity this uses an id to get the customer. I tried to use the above method where you find the customer by id, but did not see why that would be necessary when I could just pass an id to the query. It returns the customerGenre after retrieving the data. 

## Authors
Aleeza Munir
 in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
