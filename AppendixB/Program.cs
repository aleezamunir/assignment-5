﻿using AppendixB.Models;
using AppendixB.Repositories;

namespace AppendixB
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello, World!");
            ICustomerRepository repository = new CustomerRepository();
            TestCustomerGenre(repository);
        }

        static void TestSelectAll(ICustomerRepository customerRepository)
        {
            PrintCustomers(customerRepository.GetAllCustomers());
        }

        static void TestSelectPage(ICustomerRepository customerRepository)
        {
            PrintCustomers(customerRepository.GetCustomerPage(5,7));
        }

        static void TestSelect(ICustomerRepository customerRepository)
        {
            PrintCustomer(customerRepository.GetCustomerById(4));
        }

        static void TestSelectByName(ICustomerRepository customerRepository)
        {
            PrintCustomer(customerRepository.GetCustomerByName("Astrid Gruber"));
        }
        static void PrintCustomers(IEnumerable<Customer> customers)
        {
            foreach (var customer in customers)
            {
                PrintCustomer(customer);
            }
        }

        static void PrintCustomer(Customer customer)
        {
            Console.WriteLine($"--- {customer.CustomerId} {customer.Firstname} {customer.Lastname} {customer.Country} {customer.PostalCode} {customer.Number} {customer.Email} ---");
        }

        static void TestCountriesAndNumberOfCustomers(ICustomerRepository customerRepository)
        {
            PrintCountriesAndCount(customerRepository.ReturnNumberOfCustomerInEachCountry());
        }

        static void PrintCountriesAndCount(IEnumerable<CustomerCountry> customers)
        {
            foreach (var customer in customers)
            {
                PrintCountry(customer);
            }
        }

        static void PrintCountry(CustomerCountry country)
        {
            Console.WriteLine($"--- {country.Country} {country.NumOfCustomers} ---");
        }

        static void TestCustomerSpender(ICustomerRepository customerRepository)
        {
            PrintCustomerSpenders(customerRepository.GetHighestSpenders());
        }

        static void PrintCustomerSpenders(IEnumerable<CustomerSpender> customers)
        {
            foreach (var customer in customers)
            {
                PrintCustomerSpender(customer);
            }
        }

        static void PrintCustomerSpender(CustomerSpender spender)
        {
            Console.WriteLine($"--- {spender.Name} {spender.Spending} ---");
        }

        static void TestCustomerGenre(ICustomerRepository customerRepository)
        {
            int id = 4;
            PrintCustomerGenre(customerRepository.GetPopularGenre(id));
        }

        static void PrintCustomerGenre(CustomerGenre customerGenre)
        {
            Console.WriteLine($"--- {customerGenre.Name} {customerGenre.Genre} ---");
        }

        static void TestAddNewCustomer(ICustomerRepository customerRepository)
        {
            Customer test = new Customer()
            { 
                Firstname = "Aleeza",
                Lastname = "Munir",
                Country = "Sweden",
                PostalCode = "0277",
                Number = "99009900",
                Email = "aleeza@nothing.no"
            };
            if (customerRepository.AddNewCustomer(test))
            {
                Console.WriteLine("New Customer Added");
                PrintCustomer(customerRepository.GetCustomerById(60));
            } else
            {
                Console.WriteLine("Something went wrong");
            }
        }

        static void TestUpdateCustomer(ICustomerRepository customerRepository)
        {
            Customer update = new Customer()
            {
                CustomerId = 60,
                Firstname = "Aleeza",
                Lastname = "Munirr",
                Country = "Denmark",
                PostalCode = "0277",
                Number = "99009900",
                Email = "aleeza@nothing.no"
            };
            if (customerRepository.UpdateCustomer(update))
            {
                Console.WriteLine("Customer Updated");
                PrintCustomer(customerRepository.GetCustomerById(60));
            }
            else
            {
                Console.WriteLine("Something went wrong");
            }
        }
    }
}