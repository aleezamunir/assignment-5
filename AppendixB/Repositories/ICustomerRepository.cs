﻿using AppendixB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppendixB.Repositories
{
    //Repository with methods that needs to be implemented 
    public interface ICustomerRepository
    {
        public List<Customer> GetAllCustomers();

        public List<Customer> GetCustomerPage(int limit, int offset);

        public Customer GetCustomerById(int id);

        public Customer GetCustomerByName(string name);

        public bool AddNewCustomer(Customer customer);

        public bool UpdateCustomer(Customer customer);

        public List<CustomerCountry> ReturnNumberOfCustomerInEachCountry();

        public List<CustomerSpender> GetHighestSpenders();

        public CustomerGenre GetPopularGenre(int id);
    }
}
