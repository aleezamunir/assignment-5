﻿using AppendixB.Models;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppendixB.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        public bool AddNewCustomer(Customer customer)
        {
            bool success = false; //Bool value to keep track if the query was executed

            string sql = "INSERT INTO Customer(FirstName, LastName, Country, PostalCode, Phone, Email)" +
                "VALUES (@FirstName, @LastName, @Country, @PostalCode, @Number, @Email)";
            try
            {
                using (SqlConnection con = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    con.Open();
                    Console.WriteLine("Done. Adding new Customer");

                    using (SqlCommand cmd = new SqlCommand(sql, con))
                    {
                       //Adding values to the Insert statement with values from customer object
                        cmd.Parameters.AddWithValue("@FirstName", customer.Firstname);
                        cmd.Parameters.AddWithValue("@LastName", customer.Lastname);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Number", customer.Number);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);
                        success = cmd.ExecuteNonQuery() > 0 ? true: false;
                    }
                }
            } catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return success;
        }
        public bool UpdateCustomer(Customer customer)
        {
            bool success = false; //Bool to keep track if the query was executed

            string sql = "UPDATE Customer " +
                "SET FirstName = @FirstName, LastName = @LastName, Country = @Country," +
                " PostalCode = @PostalCode, Phone = @Number, Email = @Email " +
                "WHERE CustomerId = @CustomerId";

            try
            {
                using (SqlConnection con = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    con.Open();
                    Console.WriteLine("Done. Updating Customer");

                    using (SqlCommand cmd = new SqlCommand(sql, con))
                    {
                        //Parameters which are used in the update statement with values from the customer object
                        cmd.Parameters.AddWithValue("@CustomerId", customer.CustomerId);
                        cmd.Parameters.AddWithValue("@FirstName", customer.Firstname);
                        cmd.Parameters.AddWithValue("@LastName", customer.Lastname);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Number", customer.Number);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);
                        
                        success = cmd.ExecuteNonQuery() > 0 ? true:false ;
                    }
                }
            } catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }

            return success;
        }
        public List<Customer> GetAllCustomers()
        {
            List<Customer> customers = new List<Customer>(); //Make a list that'll will hold all the customer objects
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer";

            try
            {
                //Connect 
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    Console.WriteLine("Done. Getting all customers");
                    //Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {   //Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //Handle result
                                //Make a temporary customer object to add to the list
                                Customer temp = new Customer();
                                //Adding values from the reader
                                temp.CustomerId = reader.GetInt32(0);
                                temp.Firstname = reader.GetString(1);
                                temp.Lastname = reader.GetString(2);
                                //Checks if the value from the reader is null
                                if (!reader.IsDBNull(3))
                                {
                                    temp.Country = reader.GetString(3);
                                }
                                if (!reader.IsDBNull(4))
                                {
                                    temp.PostalCode = reader.GetString(4);
                                }
                                if (!reader.IsDBNull(5))
                                {
                                    temp.Number = reader.GetString(5);
                                }
                                
                                temp.Email = reader.GetString(6);
                                customers.Add(temp);
                            }
                        }
                    }
                }
                
            } catch (SqlException ex)
            {
                //Log error
                Console.WriteLine(ex.Message);
            }
            return customers;
        }

        public Customer GetCustomerById(int id)
        {
            Customer customer = new Customer(); //Make an object that will be displayed later
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE CustomerId = @CustomerId";
            try
            {
                using (SqlConnection con = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    con.Open();
                    Console.WriteLine("Done. Getting customer by id");

                    using(SqlCommand cmd = new SqlCommand(sql, con))
                    {
                        //Uses the id from the id parameter on line 136 to get the customer that we're looking for
                        cmd.Parameters.AddWithValue("@CustomerId", id);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //Adds value to the customer object 
                                customer.CustomerId = reader.GetInt32(0);
                                customer.Firstname = reader.GetString(1);
                                customer.Lastname = reader.GetString(2);
                                if (!reader.IsDBNull(3))
                                {
                                    customer.Country = reader.GetString(3);
                                }
                                if (!reader.IsDBNull(4))
                                {
                                    customer.PostalCode = reader.GetString(4);
                                }
                                if (!reader.IsDBNull(5))
                                {
                                    customer.Number = reader.GetString(5);
                                }

                                customer.Email = reader.GetString(6);
                            }
                        }   
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customer;
        }

        public Customer GetCustomerByName(string name)
        {
            Customer customer = new Customer(); //Makes a customer object. 
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE FirstName = @FirstName AND LastName = @LastName";
            try
            {
                using (SqlConnection con = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    con.Open();
                    Console.WriteLine("Done. Getting customer by id");

                    using (SqlCommand cmd = new SqlCommand(sql, con))
                    {
                        //Splits the string so we can add it to the query
                        string[] strings = name.Split(" ");
                        cmd.Parameters.AddWithValue("@FirstName", strings[0]);
                        cmd.Parameters.AddWithValue("@LastName", strings[1]);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //Adds value to the customer object
                                customer.CustomerId = reader.GetInt32(0);
                                customer.Firstname = reader.GetString(1);
                                customer.Lastname = reader.GetString(2);
                                if (!reader.IsDBNull(3))
                                {
                                    customer.Country = reader.GetString(3);
                                }
                                if (!reader.IsDBNull(4))
                                {
                                    customer.PostalCode = reader.GetString(4);
                                }
                                if (!reader.IsDBNull(5))
                                {
                                    customer.Number = reader.GetString(5);
                                }

                                customer.Email = reader.GetString(6);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customer;
        }

        public List<Customer> GetCustomerPage(int limit, int offset)
        {
            List<Customer> customers = new List<Customer>(); //Makes a list of customers to display
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer ORDER BY CustomerId OFFSET @Offset ROWS FETCH FIRST @Limit ROWS ONLY";

            try
            {
                //Connect 
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    Console.WriteLine("Done. Getting all customers");
                    //Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {   //Reader
                        //Adds limit and offset to the query
                        cmd.Parameters.AddWithValue("@Limit", limit);
                        cmd.Parameters.AddWithValue("@Offset", offset);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //Handle result
                                Customer temp = new Customer();
                                temp.CustomerId = reader.GetInt32(0);
                                temp.Firstname = reader.GetString(1);
                                temp.Lastname = reader.GetString(2);
                                if (!reader.IsDBNull(3))
                                {
                                    temp.Country = reader.GetString(3);
                                }
                                if (!reader.IsDBNull(4))
                                {
                                    temp.PostalCode = reader.GetString(4);
                                }
                                if (!reader.IsDBNull(5))
                                {
                                    temp.Number = reader.GetString(5);
                                }

                                temp.Email = reader.GetString(6);
                                customers.Add(temp);
                            }
                        }
                    }
                }

            }
            catch (SqlException ex)
            {
                //Log error
                Console.WriteLine(ex.Message);
            }
            return customers;
        }

        public List<CustomerSpender> GetHighestSpenders()
        {
            List<CustomerSpender> customerSpenders = new List<CustomerSpender>(); //MAkes a list of the spenders
            string sql = "SELECT c.FirstName, c.LastName, Sum(il.UnitPrice) AS spending FROM Invoice as i INNER JOIN Customer as c ON i.CustomerId = c.CustomerId INNER JOIN InvoiceLine as il ON il.InvoiceId = i.InvoiceId GROUP BY c.FirstName, c.LastName ORDER BY spending desc";
            
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    Console.WriteLine("Done. Getting highest Spenders");
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //Makes a temporary customerSpender object 
                                CustomerSpender temp = new CustomerSpender();
                                //Adding values to the Object
                                temp.Name = reader.GetString(0) + " " + reader.GetString(1);
                                temp.Spending = reader.GetDecimal(2);
                                //Adds the object to the list
                                customerSpenders.Add(temp);
                            }
                        }
                    }
                }
            } catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customerSpenders;
        }

        public CustomerGenre GetPopularGenre(int id)
        {
            CustomerGenre customerGenre = new CustomerGenre(); //Makes a new CustomerGenre object
            string sql = "SELECT c.FirstName, c.LastName, g.Name FROM Invoice as i INNER JOIN Customer as c ON i.CustomerId = c.CustomerId INNER JOIN InvoiceLine as il ON il.InvoiceId = i.InvoiceId INNER JOIN Track AS t ON t.TrackId = il.TrackId INNER JOIN Genre AS g ON g.GenreId = t.GenreId WHERE c.CustomerId = @CustomerId GROUP BY c.FirstName, c.LastName, g.Name ORDER BY Count(*) DESC OFFSET 0 ROWS FETCH FIRST 1 ROWS ONLY";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    Console.WriteLine("Done. Getting most Popular genre for given user");

                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        //Passes the id from line 327 to the query so we can get the spesific customer
                        cmd.Parameters.AddWithValue("@CustomerId", id);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //Adds value to the object
                                customerGenre.Name = reader.GetString(0) + " " + reader.GetString(1);
                                customerGenre.Genre = reader.GetString(2);
                            }
                        }
                    }
                }
            } catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customerGenre;
        }

        public List<CustomerCountry> ReturnNumberOfCustomerInEachCountry()
        {
            List<CustomerCountry> customers = new List<CustomerCountry>(); //Makes a list of the CustomerCountries

            string sql = "Select Country, Count(*) AS Num From Customer Group BY Country ORDER BY Num DESC";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    Console.WriteLine("Done. Getting Countries and amount of customers in each");

                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //Makes a temporary object
                                CustomerCountry temp = new CustomerCountry();
                                //Adds value to the object 
                                temp.NumOfCustomers = reader.GetInt32(1);
                                temp.Country = reader.GetString(0);
                                //Adds the object to the list 
                                customers.Add(temp);
                            }
                        }
                    }
                }
            } catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customers;
        }
        
    }
}
