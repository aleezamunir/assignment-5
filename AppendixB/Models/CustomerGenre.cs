﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppendixB.Models
{
    public class CustomerGenre
    {
        public string Name { get; set; }
        public string Genre { get; set; }
    }
}
