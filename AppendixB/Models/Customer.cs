﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppendixB.Models
{
    public class Customer
    {
        public int CustomerId { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string? Country { get; set; }
        public string? PostalCode { get; set; }
        public string? Number { get; set; }
        public string Email { get; set; }

    }
}
