USE SuperHerodb;
CREATE TABLE SuperheroPower(
Id int Identity(1,1) Primary key
)
;

ALTER TABLE SuperheroPower
	ADD SuperHeroId INT
    CONSTRAINT SuperHeroId
        FOREIGN KEY (SuperHeroId) 
        REFERENCES Superhero(SuperHeroId)
;
	

ALTER TABLE SuperheroPower
	ADD PowerId INT
	CONSTRAINT FK_PowerId
	FOREIGN KEY (PowerId)
	REFERENCES Power(PowerId)
	;