USE SuperHerodb;

INSERT INTO dbo.Power
VALUES ('Fire', 'Breath Fire');

INSERT INTO dbo.Power
VALUES ('Water', 'Kind of A watergun');

INSERT INTO dbo.Power
VALUES ('Zoom', 'Great eyesight');

INSERT INTO dbo.Power
VALUES ('Fly', 'Turn into dragon and fly');

INSERT INTO dbo.SuperheroPower
VALUES (1,2);

INSERT INTO dbo.SuperheroPower
VALUES (1,1);

INSERT INTO dbo.SuperheroPower
VALUES (2,4);

INSERT INTO dbo.SuperheroPower
VALUES (1,2);

INSERT INTO dbo.SuperheroPower
VALUES (3,1);

INSERT INTO dbo.SuperheroPower
VALUES (3,3);

INSERT INTO dbo.SuperheroPower
VALUES (2,3);