USE SuperHerodb;
ALTER TABLE dbo.Assistant
    ADD SuperHeroId INT
    CONSTRAINT FK_SuperHeroId
        FOREIGN KEY (SuperHeroId) 
        REFERENCES Superhero(SuperHeroId)
;