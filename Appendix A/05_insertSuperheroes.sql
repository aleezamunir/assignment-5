USE SuperHerodb;

INSERT INTO dbo.Superhero
VALUES ('Cardinal', 'Card', 'USA');

INSERT INTO dbo.Superhero
VALUES ('Captain America', 'Cap', 'Brooklyn');

INSERT INTO dbo.Superhero
VALUES ('Natasha Romanoff', 'Black Widow', 'Russia');