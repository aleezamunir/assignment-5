USE SuperHerodb;

CREATE TABLE Superhero (
	SuperHeroId int IDENTITY(1,1) PRIMARY KEY,
	Name varchar(50),
	Alias varchar(50),
	Origin varchar(50)
	);

CREATE TABLE Assistant (
	AssistantId int IDENTITY(1,1) PRIMARY KEY, 
	Name varchar(50)
	);

CREATE TABLE Power (
	PowerId int IDENTITY(1,1) PRIMARY KEY,
	Name varchar(50),
	Description varchar(150)
	);